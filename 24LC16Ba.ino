#include <Wire.h>
#include <string.h>

#define GOT_HERE()  do {Serial.print('['); Serial.print(__LINE__); Serial.println(']'); Serial.flush();} while(0)
#define SDA_PIN 4
#define SCL_PIN 5

// define memory address of blocks
#define I2C_SLAVE_ADDR0 0x50
#define I2C_SLAVE_ADDR1 0x51
#define I2C_SLAVE_ADDR2 0x52
#define I2C_SLAVE_ADDR3 0x53
#define I2C_SLAVE_ADDR4 0x54
#define I2C_SLAVE_ADDR5 0x55
#define I2C_SLAVE_ADDR6 0x56
#define I2C_SLAVE_ADDR7 0x57

#define PAGE_BUFFER 16
#define NUM_OF_PAGES 16
#define MAX_MEMORY 255

#define debug 1

bool ok = 0;
char addrpk[] = "{"
                "\"bc1qjaz2248t09y98vaqguq2560t7kclhesapce5sf\""
                ":"
                "\"KxdK7M6b148NsEyH6EvR91xSWN27uzCxYfhgdRznZEZfX7Kkr9Rh\""
                /* ","
                "\"bc1qjaz2248t09y98vaqguq2560t7kclhesapce5sa\""
                ":"
                "\"KxdK7M6b148NsEyH6EvR91xSWN27uzCxYfhgdRznZEZfX7Kkr9Ra\"" */
     "}";

byte internalAddr = 0x00;

void readBytes(uint8_t intaddr, uint16_t dataln){
    char singleChar;
    uint16_t req = 128;

    if(dataln <= 128){
        Wire.beginTransmission(I2C_SLAVE_ADDR7);
        Wire.write(intaddr);
        Wire.endTransmission();
        Wire.requestFrom(I2C_SLAVE_ADDR7, (int) (dataln));
        for(uint8_t i = 0; i < dataln; i++){
            singleChar = Wire.read();
            Serial.print(singleChar);
        }
    }else{
        for(uint8_t i = 0; i <= (dataln + i - 1) / dataln; i++){
            Wire.beginTransmission(I2C_SLAVE_ADDR7);
            Wire.write(intaddr + i * 128);
            Wire.endTransmission();
            Wire.requestFrom(I2C_SLAVE_ADDR7, (int) (req));
            for(uint8_t j = 0; j < req; j++){
                singleChar = Wire.read();
                Serial.print(singleChar);
            }
            req = dataln - 128;
        }
    }
}

uint8_t writeEEPROM(byte blkaddr, byte intaddr, char* data, uint16_t msglen){
    uint8_t bytes_left_in_page = 0;
    uint8_t data_fit = 0, last_write_size = 0;
    uint8_t pages_needed = 0, page_number = 0;
    uint8_t next_addr_to_write = 0, tmp = 0, remaining_pages = 0;
    uint16_t avail_eemem = 0;

    // Given the address to write to, we need to know in which page
    // the target address is located in terms of pages, starting at page [0]
    page_number = intaddr / PAGE_BUFFER;
    // Ignoring the decimal part of the result, means we
    // still have some bytes available in this page (step below follow).

    // We need to know how much space is available in the current page
    // from step above where we ignored the decimal part of the result
    bytes_left_in_page = PAGE_BUFFER - (intaddr % PAGE_BUFFER);
    // This will be used to write byte-by-byte until the end of the page,
    // then we can write entire pages at once.

    // For 24LC16B, there are more x pages available for write
    // including the page where the target address actually is
    remaining_pages = NUM_OF_PAGES - page_number - 1;

    // Check how much memory there is available, starting from the
    // target address
    avail_eemem = remaining_pages * PAGE_BUFFER + bytes_left_in_page;

    // Check if data fits the eeprom block memory capacity, starting from
    // target address - [1] doesn't fit, [0] it fits
    data_fit = msglen > avail_eemem ? 1 : 0;
        
    // Calculate size of last write
    last_write_size = (msglen - bytes_left_in_page) % PAGE_BUFFER;
    
    // Check how many pages we need to write the string
    // pages_needed = msglen % PAGE_BUFFER + 1;
    pages_needed = msglen / PAGE_BUFFER;

    #if debug
        Serial.print("Internal Addr: 0x");
        Serial.println(internalAddr, HEX);
        Serial.print("page_number: ");
        Serial.println(page_number);
        Serial.print("bytes_left_in_page: ");
        Serial.println(bytes_left_in_page);
        Serial.print("remaining_pages: ");
        Serial.println(remaining_pages);
        Serial.print("avail_eemem: ");
        Serial.println(avail_eemem);
        Serial.print("data_fit: ");
        Serial.println(data_fit);
        Serial.print("last_write_size: ");
        Serial.println(last_write_size);
        Serial.print("pages_needed: ");
        Serial.println(pages_needed);
        Serial.print("Data length: ");
        Serial.println(datalength(addrpk));
    #endif

    // Check if data fits eeprom block memory
    // then check if target address is at the beginning or at
    // the end of a page. If it is, we write 16 bytes at once
    // and then, we keep writting the remaining string
    // always 16 bytes at a time, until last 16 whole bytes,
    // then we just write the remaining final bytes of the array

    if( !data_fit ){
       if ( !bytes_left_in_page || !(bytes_left_in_page % PAGE_BUFFER)){ 
            // If we enter here, it's because we are at the very end of a
            // page or at the beginning of a page so, we can write whole pages at once
            for(uint8_t i = 0; i < pages_needed + 1; i++){ // <-- we use +1 here because the calculated
                // pages is an integer division and the length of the string / PAGE_BUFFER is not integer
                // number, so one more page is needed to write the remaining bytes of the last page
                Wire.beginTransmission(blkaddr);
                Wire.write(intaddr + i * PAGE_BUFFER); // <-- (next) target addr
                if(i < pages_needed)
                    Wire.write(&data[i * PAGE_BUFFER ], PAGE_BUFFER); // <-- 16bytes of actual data
                else
                    Wire.write(&data[i * PAGE_BUFFER ], last_write_size); // <-- last bytes of the string
                Wire.endTransmission();
                delay(5); // delay for page write
            }
            return 0;
        }else{ 
            // We are not at the beginning or end of any page
            // Fill the remaining bytes of the current page
            Wire.beginTransmission(blkaddr);
            Wire.write(intaddr);
            Wire.write(data, bytes_left_in_page);
            Wire.endTransmission();
            delay(5); // delay for page write
            
            next_addr_to_write = intaddr + bytes_left_in_page;
            // Serial.print("Next address: 0x");
            // Serial.println(next_addr_to_write, HEX);
            // Fill next (complete) pages with 16 bytes
           Wire.beginTransmission(blkaddr);
            for(uint8_t i = 0; i < pages_needed - 1; i++){
                Wire.write(next_addr_to_write + i * PAGE_BUFFER);
                Wire.write(&data[(bytes_left_in_page + i * PAGE_BUFFER)],
                           PAGE_BUFFER);
                Wire.endTransmission();
                delay(5); // delay for page write
                if(i < pages_needed)
                    // if there are still pages to write, start transmission again,
                    // otherwise, don't
                    Wire.beginTransmission(blkaddr);
                tmp = i + 1; // Used to extend the scope of variable 'i'
                             // out of the for loop
            }
            // Last write
            Wire.beginTransmission(blkaddr);
            Wire.write(next_addr_to_write + tmp * PAGE_BUFFER);
            Wire.write(&data[msglen - last_write_size], last_write_size + 1);
            Wire.endTransmission();
            delay(5); // delay for page write
        }
        return 0;
    }else{
        // Turn on a red led because data is larger than the 
        // available eeprom memory
        Serial.println("Not enough memory available!");
        return -1;
    }
}
uint16_t datalength(char* str){
   uint8_t msglen = 0; 
   // Compute the length of the string
    do{
       msglen++;
    }while(str[msglen]);
    return msglen;
    // For the given string in line 25, this should return 101
}

void erase_eeprom(){
    Wire.write(0x00);
    for(uint8_t i = 0; i < 0xff; i++){
        Wire.write(0xff);
    }
    Wire.endTransmission();
}

void setup(){
    Serial.begin(115200);
    Wire.begin();
    Wire.beginTransmission(I2C_SLAVE_ADDR7);
}

void loop(){
    if(!ok){
        // erase_eeprom();
        if(!writeEEPROM(I2C_SLAVE_ADDR7, internalAddr, addrpk, datalength(addrpk)))
            // readEEPROM(internalAddr, datalength(addrpk));
            readBytes(internalAddr, datalength(addrpk));
        ok = 1;
    }
}
